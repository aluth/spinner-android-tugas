package id.ac.lpkia.pabsesi3tugas1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Spinner operator;
    EditText angka1, angka2;
    Button button;
    TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator_spinner);

        operator = findViewById(R.id.operator);
        angka1 = findViewById(R.id.angka1);
        angka2 = findViewById(R.id.angka2);
        button = findViewById(R.id.button);
        hasil = findViewById(R.id.hasil);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.operator1, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        operator.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String angkaPertama = angka1.getText().toString();
                String angkaKedua = angka2.getText().toString();
                int number1 = Integer.parseInt(angkaPertama);
                int number2 = Integer.parseInt(angkaKedua);
                String operatorBilangan = operator.getSelectedItem().toString();

                if(operatorBilangan.equalsIgnoreCase("Perkalian")){
                    int hasilnya = number1 * number2;
                    String hasilnya1 = new Integer(hasilnya).toString();
                    hasil.setText(hasilnya1);
                } else if(operatorBilangan.equalsIgnoreCase("Pembagian")){
                    int hasilnya = number1 / number2;
                    String hasilnya1 = new Integer(hasilnya).toString();
                    hasil.setText(hasilnya1);
                } else if(operatorBilangan.equalsIgnoreCase("Penjumlahan")){
                    int hasilnya = number1 + number2;
                    String hasilnya1 = new Integer(hasilnya).toString();
                    hasil.setText(hasilnya1);
                } else {
                    int hasilnya = number1 - number2;
                    String hasilnya1 = new Integer(hasilnya).toString();
                    hasil.setText(hasilnya1);
                }

            }
        });
    }
}
